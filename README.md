# Демо приложение Flask-mysql-demo

Демо приложение flask-mysql-demo[https://github.com/a2975667/flask-gcp-mysql-demo] разработанное для курса CS411

### Запуск базы
```bash
docker run -d --name mysql \
  -e ALLOW_EMPTY_PASSWORD=yes \
  -e MYSQL_USER=user \
  -e MYSQL_PASSWORD=password \
  -e MYSQL_DATABASE=todo \
  -e MYSQL_AUTHENTICATION_PLUGIN=mysql_native_password \
  -p 3306:3306 \
  bitnami/mysql:8.0.34

```

### Создание таблицы
```sql
CREATE TABLE tasks (
    id int NOT NULL AUTO_INCREMENT,
    task varchar(255) NOT NULL,
    status char(30),
    PRIMARY KEY (id)
);

INSERT INTO tasks (task, status) VALUES ("task no.1" , "Todo");
INSERT INTO tasks (task, status) VALUES ("task no.2" , "Todo");
INSERT INTO tasks (task, status) VALUES ("task no.3" , "Todo");
```

### Сборка и запуск
```bash
# Сборка
docker build -t todo:latest .
# Запуск
docker run -d \
  -e MYSQL_USER=user \
  -e MYSQL_PASSWORD=password \
  -e MYSQL_DB=todo \
  -e MYSQL_HOST=mysql \
  -p 8000:5000 \
  todo:latest
```

### Локальный запуск
```bash
export MYSQL_USER=user
export MYSQL_PASSWORD=password
export MYSQL_DB=todo
export MYSQL_HOST=172.17.0.2
python main.py
```
