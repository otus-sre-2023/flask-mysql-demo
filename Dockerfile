ARG BASE_IMAGE=python:3.8-slim-buster
FROM ${BASE_IMAGE}

WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
EXPOSE 5000
ENV DEBUG_METRICS=True
ENV DEBUG=True
CMD python main.py